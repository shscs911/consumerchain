import * as firebase from "firebase";
import "firebase/auth";
import "firebase/firestore";

const config = {
  apiKey: "AIzaSyDYjabQlFjjW8k4n3RC_pZSUHb78lhb8qo",
  authDomain: "consumerchain.firebaseapp.com",
  databaseURL: "https://consumerchain.firebaseio.com",
  projectId: "consumerchain",
  storageBucket: "consumerchain.appspot.com",
  messagingSenderId: "81232049112",
  appId: "1:81232049112:web:6e201681ee778831e238ad"
};

export default !firebase.apps.length
  ? firebase.initializeApp(config)
  : firebase.app();
