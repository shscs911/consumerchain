import React, { Component } from "react";
import { StyleSheet, Text, View, Image } from "react-native";

import firebase from "../firebase";

let data;

const db = firebase.firestore();

export default class ProfileView extends Component {
  /*  constructor(props) {
    super(props);
    this.state = {
      users: []
    };
  } */

  componentDidUpdate = mail => {
    db.collection("consumers")
      .where("email", "==", mail)
      .get()
      .then(snapshot => {
        snapshot.forEach(doc => {
          data = doc.data();
          console.log("Email", mail);
          console.log(doc.id, "=>", doc.data());

          //this.setState({ users: data });
        });
      })
      .catch(err => {
        console.log("Error getting documents", err);
      });
  };
  render() {
    //const { users } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerContent}>
            <Image
              style={styles.avatar}
              source={{
                uri: "https://bootdey.com/img/Content/avatar/avatar1.png"
              }}
            />

            <Text style={styles.name}>
              Email: {this.props.navigation.state.params.email}
            </Text>
          </View>
          {
            (temp = this.componentDidUpdate(
              this.props.navigation.state.params.email
            ))
          }
        </View>

        <View style={styles.body}>
          <View style={styles.bodyContent}>
            <Text style={styles.textInfo}>Name: {JSON.stringify(data)}</Text>

            <Text style={styles.textInfo}>Following: 244</Text>

            <Text style={styles.textInfo}>Followers: 1.250</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: "#43A047"
  },
  headerContent: {
    padding: 30,
    alignItems: "center"
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10
  },
  name: {
    fontSize: 22,
    color: "#FFFFFF",
    fontWeight: "600"
  },
  bodyContent: {
    flex: 1,
    alignItems: "center",
    padding: 30
  },
  textInfo: {
    fontSize: 18,
    marginTop: 20,
    color: "#696969"
  }
});
