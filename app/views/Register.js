import React, { Component } from "react";

import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert,
  AsyncStorage
} from "react-native";

import firebase from "../firebase";

const db = firebase.firestore();

export default class SignUpView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: "",
      email: "",
      phno: "",
      password: ""
    };
  }

  handleName = text => {
    this.setState({ fullName: text });
  };
  handleEmail = text => {
    this.setState({ email: text });
  };
  handlePhone = text => {
    this.setState({ phno: text });
  };
  handlePassword = text => {
    this.setState({ password: text });
  };

  register = (email, pass, phno, name) => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, pass)
      .then(function() {
        db.collection("consumers")
          .doc()
          .set({
            email: email,
            phone: phno,
            name: name
          });
      })
      .catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
      });

    this.props.navigation.navigate("Login");
  };

  onClickListener = viewId => {
    Alert.alert("Alert", "Button pressed " + viewId);
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <Image
            style={styles.inputIcon}
            source={{
              uri: "https://png.icons8.com/male-user/ultraviolet/50/3498db"
            }}
          />
          <TextInput
            style={styles.inputs}
            placeholder="Name"
            keyboardType="default"
            underlineColorAndroid="transparent"
            onChangeText={this.handleName}
          />
        </View>

        <View style={styles.inputContainer}>
          <Image
            style={styles.inputIcon}
            source={{
              uri: "https://img.icons8.com/ultraviolet/50/000000/email.png"
            }}
          />
          <TextInput
            style={styles.inputs}
            placeholder="Email"
            keyboardType="email-address"
            underlineColorAndroid="transparent"
            onChangeText={this.handleEmail}
          />
        </View>

        <View style={styles.inputContainer}>
          <Image
            style={styles.inputIcon}
            source={{
              uri: "https://img.icons8.com/ultraviolet/50/000000/android.png"
            }}
          />
          <TextInput
            style={styles.inputs}
            placeholder="Phone number"
            keyboardType="number-pad"
            underlineColorAndroid="transparent"
            onChangeText={this.handlePhone}
          />
        </View>

        <View style={styles.inputContainer}>
          <Image
            style={styles.inputIcon}
            source={{
              uri: "https://img.icons8.com/ultraviolet/50/000000/password.png"
            }}
          />
          <TextInput
            style={styles.inputs}
            placeholder="Password"
            secureTextEntry={true}
            underlineColorAndroid="transparent"
            onChangeText={this.handlePassword}
          />
        </View>

        <TouchableHighlight
          style={[styles.buttonContainer, styles.signupButton]}
          onPress={() =>
            this.register(
              this.state.email,
              this.state.password,
              this.state.phno,
              this.state.fullName
            )
          }
        >
          <Text style={styles.signUpText}>Sign up</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#43A047"
  },
  inputContainer: {
    borderBottomColor: "#F5FCFF",
    backgroundColor: "#FFFFFF",
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: "row",
    alignItems: "center"
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: "#FFFFFF",
    flex: 1
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginLeft: 15,
    justifyContent: "center"
  },
  buttonContainer: {
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    width: 250,
    borderRadius: 30
  },
  signupButton: {
    backgroundColor: "#00b5ec"
  },
  signUpText: {
    color: "white"
  }
});
