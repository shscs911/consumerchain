## Install Instructions

- `git clone gitlab.com/shscs911/consumerchain.git`
- `cd consumerchain`
- `npm install -g expo-cli` (Use `sudo` if permission is denied)
- `npm install`
- `expo start --localhost`
- Use android emulator or physical android device to test the application:
	- https://docs.expo.io/versions/latest/introduction/installation/
- App navigation: Register => Login => Profile	
