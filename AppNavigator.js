import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import Login from "./app/views/Login";
import Register from "./app/views/Register";
import Profile from "./app/views/Profile";

const AppNavigator = createStackNavigator(
  {
    Login: Login,
    Register: Register,
    Profile: Profile
  },
  {
    headerMode: "none"
  }
);

const App = createAppContainer(AppNavigator);

export default App;
