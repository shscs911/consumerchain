import React from "react";
import AppNavigator from "./AppNavigator.js";

export default class App extends React.Component {
  render() {
    return <AppNavigator />;
  }
}
